package net.openesb.netbeans.module.server.support.standalone.ide;

import javax.enterprise.deploy.spi.DeploymentManager;
import net.openesb.netbeans.module.server.support.standalone.wizard.AddInstanceIterator;
import org.netbeans.modules.j2ee.deployment.plugins.spi.FindJSPServlet;
import org.netbeans.modules.j2ee.deployment.plugins.spi.IncrementalDeployment;
import org.netbeans.modules.j2ee.deployment.plugins.spi.OptionalDeploymentManagerFactory;
import org.netbeans.modules.j2ee.deployment.plugins.spi.StartServer;
import org.openide.WizardDescriptor;

/**
 *
 * @author David BRASSELY
 */
public class StandaloneOptionalFactory extends OptionalDeploymentManagerFactory {

    @Override
    public StartServer getStartServer(DeploymentManager dm) {
        return new StandaloneStartServer(dm);
    }

    @Override
    public IncrementalDeployment getIncrementalDeployment(DeploymentManager dm) {
        return null;
    }

    @Override
    public FindJSPServlet getFindJSPServlet(DeploymentManager dm) {
        return null;
    }
    
    @Override
    public WizardDescriptor.InstantiatingIterator getAddInstanceIterator() {
        return new AddInstanceIterator();
    }
    
}
