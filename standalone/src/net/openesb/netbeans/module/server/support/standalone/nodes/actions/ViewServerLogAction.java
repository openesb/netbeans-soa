package net.openesb.netbeans.module.server.support.standalone.nodes.actions;

import java.io.File;
import net.openesb.netbeans.module.server.support.standalone.StandaloneDeploymentManager;
import net.openesb.netbeans.module.server.support.standalone.ide.StandaloneOutputSupport;
import net.openesb.netbeans.module.server.support.standalone.nodes.StandaloneInstanceNode;
import net.openesb.netbeans.module.server.support.standalone.util.StandaloneProperties;
import org.netbeans.modules.j2ee.deployment.plugins.api.InstanceProperties;
import org.netbeans.modules.j2ee.deployment.plugins.api.UISupport;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.NodeAction;
import org.openide.windows.InputOutput;

/**
 * Open OpenESB standalone output in the output window.
 * 
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class ViewServerLogAction extends NodeAction {

    @Override
    protected boolean enable(Node[] activatedNodes) {
	return true;
    }
    
    @Override
    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }
    
    @Override
    public String getName() {
        return NbBundle.getMessage(ViewServerLogAction.class, "LBL_ServerLogAction"); // NOI18N
    }
    
    @Override
    protected boolean asynchronous() {
        return false;
    }
    
    @Override
    protected void performAction(Node[] activatedNodes) {
	for (Node activatedNode : activatedNodes) {
            Object node = activatedNode.getLookup().lookup(StandaloneInstanceNode.class);
            
            if (!(node instanceof StandaloneInstanceNode)) {
                continue;
            }
            
            StandaloneDeploymentManager dm = ((StandaloneInstanceNode)node).getDeploymentManager();
            InputOutput io = UISupport.getServerIO(dm.getUri());
            if (io != null) {
                io.select();
            }
            
            InstanceProperties ip = dm.getInstanceProperties();
            StandaloneOutputSupport outputSupport = StandaloneOutputSupport.getInstance(ip, false);
            if (outputSupport == null) {
                outputSupport = StandaloneOutputSupport.getInstance(ip, true);
                String serverDir = ip.getProperty(StandaloneProperties.PROPERTY_ROOT_DIR);
                String logFileName = serverDir + File.separator + "logs" + File.separator + "server.log" ; // NOI18N
                File logFile = new File(logFileName);
                if (logFile.exists()) {
                    outputSupport.start(io, logFile);
                }                
            }
        }
    }    
}
