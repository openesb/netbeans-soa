/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package net.openesb.netbeans.module.server.support.standalone.nodes.actions;

import net.openesb.netbeans.module.server.support.standalone.nodes.StandaloneItemNode;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.NodeAction;

/**
 *
 * @author david
 */
public class RefreshAction extends NodeAction {
    
    /** Creates a new instance of Refresh */
    public RefreshAction() {
    }
    
    @Override
    protected boolean enable(org.openide.nodes.Node[] nodes) {
        RefreshCookie cookie;
	for (Node node : nodes) {
	    cookie = (RefreshCookie) node.getLookup().lookup(RefreshCookie.class);
	    if (cookie == null)
		return false;
	}
        
        return true;
    }    
    
    @Override
    public String getName() {
        return NbBundle.getMessage(StandaloneItemNode.class, "LBL_RefreshAction"); // NOI18N
    }
    
    @Override
    protected void performAction(org.openide.nodes.Node[] nodes) {
	for (Node node : nodes) {
	    RefreshCookie cookie = (RefreshCookie) node.getLookup().lookup(RefreshCookie.class);
	    if (cookie != null)
		cookie.refresh();
	}
    }
    
    @Override
    protected boolean asynchronous() {
        return false;
    }
    
    @Override
    public org.openide.util.HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }
    
}
