package net.openesb.netbeans.module.server.support.standalone;

import javax.enterprise.deploy.spi.Target;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class StandaloneTarget implements Target {

    private String name;

    private String desc;

    private String uri;

    public StandaloneTarget (String name, String desc, String uri) {
        this.name = name;
        this.desc = desc;
        this.uri = uri;
    }

    public String getName () {
        return name;
    }

    public String getDescription () {
        return desc;
    }
    
    public String getServerUri () {
        return uri;
    }
    
    public String toString () {
        return name;
    }
    
}
