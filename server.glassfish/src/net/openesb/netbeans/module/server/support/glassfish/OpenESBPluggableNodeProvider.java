package net.openesb.netbeans.module.server.support.glassfish;

import com.sun.esb.management.client.ManagementClient;
import com.sun.esb.management.client.ManagementClientFactory;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.management.MBeanServerConnection;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import org.netbeans.modules.glassfish.common.GlassfishInstance;
import org.netbeans.modules.glassfish.common.GlassfishInstanceProvider;
import org.netbeans.modules.glassfish.spi.GlassfishModule;
import org.netbeans.modules.glassfish.spi.PluggableNodeProvider;
import org.netbeans.modules.sun.manager.jbi.nodes.api.NodeExtensionProvider;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class OpenESBPluggableNodeProvider implements PluggableNodeProvider {

    private static final Logger LOGGER = Logger.getLogger(OpenESBPluggableNodeProvider.class.getName());

    @Override
    public org.openide.nodes.Node getPluggableNode(Map<String, String> map) {
        if (map != null) {
            GlassfishInstance instance = GlassfishInstanceProvider.getGlassFishInstanceByUri(map.get(GlassfishModule.URL_ATTR));
            GlassfishModule.ServerState serverState = instance.getServerState();

            if (serverState == GlassfishModule.ServerState.RUNNING
                    || serverState == GlassfishModule.ServerState.RUNNING_JVM_DEBUG
                    || serverState == GlassfishModule.ServerState.RUNNING_JVM_PROFILER) {
                
                MBeanServerConnection mbsc = createMBeanServerConnection(instance);

                for (NodeExtensionProvider nep : Lookup.getDefault().lookupAll(NodeExtensionProvider.class)) {
                    if (nep != null) {
                        Node node = nep.getExtensionNode(mbsc);
                        if (node != null) {
                            return node;
                        }
                    }
                }
            }
        }

        return null;
    }

    private MBeanServerConnection createMBeanServerConnection(GlassfishInstance instance) {
        try {
            ManagementClient client = ManagementClientFactory.getInstance(
                    instance.getHost(), 8686, instance.getAdminUser(), 
                    instance.getAdminPassword());
            Field conField = client.getClass().getDeclaredField("remoteConnection");
            conField.setAccessible(true);
            
            return (MBeanServerConnection) conField.get(client);
        } catch (Exception ex) {
            LOGGER.log(Level.INFO, ex.getMessage(), ex);
            return null;
        }
    }

}
