package org.netbeans.modules.sun.manager.jbi.util;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author David BRASSELY
 */
public abstract class AbstractServerInstanceFactory implements ServerInstanceFactory {

    protected final static String DISPLAY_NAME = "displayName"; // NOI18N
    protected final static String PASSWORD = "password"; // NOI18N
    protected final static String URL = "url"; // NOI18N
    protected final static String USER_NAME = "username"; // NOI18N
    
    @Override
    public boolean canHandle(Node instanceNode) {
        NodeList childNodes = instanceNode.getChildNodes();

        for (int j = 0; j < childNodes.getLength(); j++) {
            Node childNode = childNodes.item(j);
            String childNodeName = childNode.getNodeName();

            if ((childNode.getNodeType() == Node.ELEMENT_NODE)
                    && (childNodeName.equalsIgnoreCase("attr"))) { // NOI18N
                Element attrElement = (Element) childNode;

                String key = attrElement.getAttribute("name");  // NOI18N
                String value = attrElement.getAttribute("stringvalue"); // NOI18N

                if (key.equalsIgnoreCase(DISPLAY_NAME)) {
                    return canHandle(value);
                }
            }
        }
        
        return false;
    }

    public abstract boolean canHandle(String instanceDisplayName);
}
