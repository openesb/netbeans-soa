package org.netbeans.modules.sun.manager.jbi.util;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.security.AllPermission;
import java.security.CodeSource;
import java.security.PermissionCollection;
import java.security.Permissions;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NameNotFoundException;
import javax.naming.NamingException;
import org.openide.util.Exceptions;

/**
 *
 * @author David BRASSELY
 */
public class JBossServerInstance extends ServerInstance {

    private static final Logger LOGGER = Logger.getLogger(JBossServerInstance.class.getName());

    private static final String MODULES_BASE_7 = "modules" + File.separator + "system"
            + File.separator + "layers" + File.separator + "base" + File.separator;

    @Override
    public List<File> getClasses() {
        List<File> classes = new ArrayList<File>();
        String serverLocation = this.getUrlLocation();

        String jbi_rt_jar = serverLocation + "/modules/net/openesb/jboss7/main/jbi_rt.jar";

        File jbi_rt_jarFile = new File(jbi_rt_jar);
        if (jbi_rt_jarFile.exists()) {
            classes.add(jbi_rt_jarFile);
        } else {
            throw new RuntimeException("JbiClassLoader: Cannot find "
                    + jbi_rt_jar + ".");
        }

        return classes;
    }

    @Override
    public boolean isJBIEnabled(MBeanServerConnection mBeanServerConnection) {
        try {
            return mBeanServerConnection.isRegistered(new ObjectName(
                    "com.sun.jbi:ServiceName=ESBAdministrationService,ComponentType=System"));
        } catch (Exception ex) {
            return false;
        }
    }

    @Override
    public MBeanServerConnection getConnection() {

        ClassLoader oldLoader = Thread.currentThread().getContextClassLoader();
        InitialContext ctx = null;
        JMXConnector conn = null;

        try {
            URLClassLoader loader = getClassloader();
            Thread.currentThread().setContextClassLoader(loader);

            Properties env = new Properties();

            // Sets the jboss naming environment
            String jnpPort = getAdminPort();

            env.put(Context.INITIAL_CONTEXT_FACTORY, "org.jboss.naming.NamingContextFactory");
            env.put(Context.PROVIDER_URL, "jnp://localhost" + ":" + jnpPort);
            env.put(Context.OBJECT_FACTORIES, "org.jboss.naming");
            env.put(Context.URL_PKG_PREFIXES, "org.jboss.naming:org.jnp.interfaces");
            env.put("jnp.disableDiscovery", Boolean.TRUE);

            // TODO: whre to find these informations ?
            setUserName("admin");
            setPassword("admin");
            
            env.put(Context.SECURITY_PRINCIPAL, getUserName());
            env.put(Context.SECURITY_CREDENTIALS, getPassword());
            env.put("jmx.remote.credentials", // NOI18N
                    new String[]{getUserName(), getPassword()});

            MBeanServerConnection rmiServer = null;
            try {
                JMXServiceURL url;
                url = new JMXServiceURL(
                        System.getProperty("jmx.service.url", "service:jmx:remoting-jmx://localhost:9999")); // NOI18N
                conn = JMXConnectorFactory.connect(url);

                rmiServer = conn.getMBeanServerConnection();
            } catch (IOException ex) {
                LOGGER.log(Level.FINE, null, ex);
            }

            if (rmiServer == null && ctx != null) {
                // Lookup RMI Adaptor
                rmiServer = (MBeanServerConnection) ctx.lookup("/jmx/invoker/RMIAdaptor"); // NOI18N
            }

            return rmiServer;
        } catch (NameNotFoundException ex) {
            LOGGER.log(Level.FINE, null, ex);
            return null;
        } catch (NamingException ex) {
            LOGGER.log(Level.FINE, null, ex);
            return null;
        } catch (Exception ex) {
            LOGGER.log(Level.FINE, null, ex);
            return null;
        } finally {
            Thread.currentThread().setContextClassLoader(oldLoader);
        }
    }

    private URLClassLoader getClassloader() {
        List<URL> urlList = new ArrayList<URL>();
        String sep = File.separator;

        try {
            urlList.add(
                    new File(getUrlLocation() + sep + "modules" + sep + "net" + sep + "openesb" + sep + "jboss7" + sep + "main", "jbi_rt.jar").toURI().toURL());

            File org = new File(getUrlLocation(), MODULES_BASE_7 + "org");
            File jboss = new File(org, "jboss");
            File as = new File(jboss, "as");

            urlList.add(new File(getUrlLocation(), "jboss-modules.jar").toURI().toURL());
            urlList.add(new File(getUrlLocation(), "bin" + sep + "client" + sep + "jboss-client.jar").toURI().toURL());

            addUrl(urlList, jboss, "logging" + sep + "main", Pattern.compile("jboss-logging-.*.jar"));
            addUrl(urlList, jboss, "threads" + sep + "main", Pattern.compile("jboss-threads-.*.jar"));
            addUrl(urlList, jboss, "remoting3" + sep + "main", Pattern.compile("jboss-remoting-.*.jar"));
            addUrl(urlList, jboss, "xnio" + sep + "main", Pattern.compile("xnio-api-.*.jar"));
            addUrl(urlList, jboss, "xnio" + sep + "nio" + sep + "main", Pattern.compile("xnio-nio-.*.jar"));
            addUrl(urlList, jboss, "dmr" + sep + "main", Pattern.compile("jboss-dmr-.*.jar"));
            addUrl(urlList, jboss, "msc" + sep + "main", Pattern.compile("jboss-msc-.*.jar"));
            addUrl(urlList, jboss, "common-core" + sep + "main", Pattern.compile("jboss-common-core-.*.jar"));
            addUrl(urlList, as, "ee" + sep + "deployment" + sep + "main", Pattern.compile("jboss-as-ee-deployment-.*.jar"));
            addUrl(urlList, as, "naming" + sep + "main", Pattern.compile("jboss-as-naming-.*.jar"));
            addUrl(urlList, as, "controller-client" + sep + "main", Pattern.compile("jboss-as-controller-client-.*.jar"));
            addUrl(urlList, as, "protocol" + sep + "main", Pattern.compile("jboss-as-protocol-.*.jar"));

            JBClassLoader loader = new JBClassLoader(urlList.toArray(new URL[]{}));
            return loader;

        } catch (Exception ex) {
            LOGGER.log(Level.WARNING, null, ex);
        }
        return null;
    }

    public static class JBClassLoader extends URLClassLoader {

        public JBClassLoader(URL[] urls) throws MalformedURLException, RuntimeException {
            super(urls);
        }

        protected PermissionCollection getPermissions(CodeSource codeSource) {
            Permissions p = new Permissions();
            p.add(new AllPermission());
            return p;
        }

        public Enumeration<URL> getResources(String name) throws IOException {
            // get rid of annoying warnings
            if (name.indexOf("jndi.properties") != -1) {// || name.indexOf("i18n_user.properties") != -1) { // NOI18N
                return Collections.enumeration(Collections.<URL>emptyList());
            }

            return super.getResources(name);
        }
    }

    private static void addUrl(List<URL> result, File root, String path, final Pattern pattern) {
        File folder = new File(root, path);
        File[] children = folder.listFiles(new FilenameFilter() {

            @Override
            public boolean accept(File dir, String name) {
                return pattern.matcher(name).matches();
            }
        });
        if (children != null) {
            for (File child : children) {
                try {
                    result.add(child.toURI().toURL());
                } catch (MalformedURLException ex) {
                    LOGGER.log(Level.INFO, null, ex);
                }
            }
        }
    }
}
