package org.netbeans.modules.compapp.casaeditor.properties;

import java.awt.Component;
import java.beans.PropertyEditorSupport;

/**
 * @author DavidD
 */
public class RouteTypeEditor extends PropertyEditorSupport {
    
    
    public String getAsText() {        
        return getValue().toString();
    }
    
    public Component getCustomEditor() {
        return null;
    }
    
    public void setAsText(String value) throws IllegalArgumentException {
        setValue(value);
    }
    
    public String[] getTags() {
        return new String[]{"direct","indirect","redirect"};
    }
}
