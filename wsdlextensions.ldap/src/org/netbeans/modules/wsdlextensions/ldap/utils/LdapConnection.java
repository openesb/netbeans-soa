/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2010 Oracle and/or its affiliates. All rights reserved.
 *
 * Oracle and Java are registered trademarks of Oracle and/or its affiliates.
 * Other names may be trademarks of their respective owners.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 * 
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 * 
 * Contributor(s):
 * 
 * Portions Copyrighted 2008 Sun Microsystems, Inc.
 */
package org.netbeans.modules.wsdlextensions.ldap.utils;

import com.pymma.jbi.ldapbc.util.LDAPSocketFactory;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.NameClassPair;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import javax.naming.ldap.StartTlsRequest;
import javax.naming.ldap.StartTlsResponse;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.swing.JOptionPane;
import org.netbeans.modules.wsdlextensions.ldap.LdapConnectionProperties;
import org.netbeans.modules.wsdlextensions.ldap.ldif.LdifObjectClass;
import org.openide.util.Exceptions;

/**
 *
 * @author Gary Zheng
 */
public class LdapConnection extends LdapConnectionProperties {

    public static String SSL_TYPE_NONE = "None";
    public static String SSL_TYPE_SSL = "Enable SSL";
    public static String SSL_TYPE_TLS = "TLS on demand";
    public static String AUTHENTICATION_NONE = "none";
    public static String AUTHENTICATION_SIMPLE = "simple";
//    public static int CONNECTION_CREATED_COUNT = 0;
//    public static int CONNECTION_CLOSED_COUNT = 0;
    private LdapContext connection;
    private String dn;
    private String curConnectionType = "";
    private boolean connectionReconnect = true;
    private static final Logger mLogger = Logger.getLogger(LdapConnection.class.getName());

    public LdapConnection() {
    }

    public String getDn() {
        return dn;
    }

    public void setDn(String dn) {
        this.dn = dn;
    }

    private static boolean isEmpty(String str) {
        if (null == str) {
            return true;
        }
        return str.length() == 0;
    }

    @Override
    public Object getProperty(String property) {
        Object invoke = null;
        try {
            mLogger.fine(MessageFormat.format("Get value for the properties {0}", property));
            Class cls = this.getClass();
            property = property.substring(0, 1).toUpperCase() + property.substring(1);
            Method method = cls.getMethod("get" + property);
            invoke = method.invoke(this, (java.lang.Object[]) null);
            mLogger.fine(MessageFormat.format("Get property {0} successfully", property));
            // Catch sequence must be changed to multiple catch with migration to higher 
        } catch (NoSuchMethodException ex) {
            mLogger.fine(MessageFormat.format("Unable to get property {0} successfully", property));
            Exceptions.printStackTrace(ex);
        } catch (SecurityException ex) {
            mLogger.fine(MessageFormat.format("Unable to get property {0} successfully", property));
            Exceptions.printStackTrace(ex);
        } catch (IllegalAccessException ex) {
            mLogger.fine(MessageFormat.format("Unable to get property {0} successfully", property));
            Exceptions.printStackTrace(ex);
        } catch (IllegalArgumentException ex) {
            mLogger.fine(MessageFormat.format("Unable to get property {0} successfully", property));
            Exceptions.printStackTrace(ex);
        } catch (InvocationTargetException ex) {
            mLogger.fine(MessageFormat.format("Unable to get property {0} successfully", property));
            Exceptions.printStackTrace(ex);
        }
        return invoke;
    }

    /**
     * This method is derived from the Component one. It is less complex since
     * many parameters linked to the component are not taken into account. ex:
     * the connection pool
     *
     * @return
     */
    public LdapContext getConnection() {

        // Reuse the Connection if the connection already exsists and if the 
        // parameters are identical                
        if (null != connection && curConnectionType.equals(this.getSsltype()) && !connectionReconnect) {
            mLogger.fine("Return existing connection");
            return connection;
        }
        mLogger.fine("Start creating connection process");
        // Close the existing connection if the Connection must be rebuilt 
        // ex: new parameters        
        closeConnection();
//        CONNECTION_CREATED_COUNT++;
        /**
         * we defined three options for creating a connection 1- No SSL or TLS
         * 2- SSL 3- TLS there is no support of strong Authentication in this
         * version In this version the SSL is only defined by the SSL Type.
         */
        // This is the LDAP context that will be return at least 
        LdapContext ldapContext = null;

        if (this.getSsltype().equals(LdapConnection.SSL_TYPE_SSL)) {
            ldapContext = this.createSSLConnection();
        } else if (this.getSsltype().equals(LdapConnection.SSL_TYPE_TLS)) {
            ldapContext = this.createTLSConnection();
        } else {
            ldapContext = this.createSimpleConnection();
        }
        connection = ldapContext;
        curConnectionType = this.getSsltype();
        connectionReconnect = false;
        return ldapContext;
    }

    /**
     * Create a connection without SSL or TLS The connection can be with the
     * authentication non or simple
     *
     * @return
     */
    private LdapContext createSimpleConnection() {
        LdapContext ret = null;
        try {
            // Enter the URL. The URL must contain the host host and port 
            // plus the root DB
            Hashtable<String, String> env = new Hashtable<String, String>();
            env.put(Context.PROVIDER_URL, this.getLocation());

            // check authentication
            if (this.getAuthentication().equalsIgnoreCase(AUTHENTICATION_SIMPLE)) {
                env.put(Context.SECURITY_AUTHENTICATION, this.getAuthentication());
            } else {
                env.put(Context.SECURITY_AUTHENTICATION, "none");
            }
            //if authentication is simple weneed to put principal and credential in the Hashmap
            if (AUTHENTICATION_SIMPLE.equalsIgnoreCase(this.getAuthentication())) {
                if (!isEmpty(this.getPrincipal())) {
                    env.put(Context.SECURITY_PRINCIPAL, this.getPrincipal());
                }
                if (!isEmpty(this.getCredential())) {
                    env.put(Context.SECURITY_CREDENTIALS, this.getCredential());
                }
            } else {
                if (!isEmpty(this.getPrincipal())) {
                    env.put(Context.SECURITY_PRINCIPAL, "");
                }
                if (!isEmpty(this.getCredential())) {
                    env.put(Context.SECURITY_CREDENTIALS, "");
                }
            }
            if (!isEmpty(this.getProtocol())) {
                env.put(Context.SECURITY_PROTOCOL, this.getProtocol());
            }
            env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
            ret = new InitialLdapContext(env, null);            
        } catch (NamingException ex) {
            Exceptions.printStackTrace(ex);
        }
        return ret;
    }

    private LdapContext createSSLConnection() {
        LdapContext ret = null;
        try {
            // Enter the URL. The URL must contain the host host and port 
            // plus the root DB
            Hashtable<String, String> env = new Hashtable<String, String>();
            env.put(Context.PROVIDER_URL, this.getLocation());

            // check authentication
            if (this.getAuthentication().equalsIgnoreCase(AUTHENTICATION_SIMPLE)) {
                env.put(Context.SECURITY_AUTHENTICATION, this.getAuthentication());
            } else {
                env.put(Context.SECURITY_AUTHENTICATION, "none");
            }
            //if authentication is simple weneed to put principal and credential in the Hashmap
            if (AUTHENTICATION_SIMPLE.equalsIgnoreCase(this.getAuthentication())) {
                if (!isEmpty(this.getPrincipal())) {
                    env.put(Context.SECURITY_PRINCIPAL, this.getPrincipal());
                }
                if (!isEmpty(this.getCredential())) {
                    env.put(Context.SECURITY_CREDENTIALS, this.getCredential());
                }
            } else {
                if (!isEmpty(this.getPrincipal())) {
                    env.put(Context.SECURITY_PRINCIPAL, "");
                }
                if (!isEmpty(this.getCredential())) {
                    env.put(Context.SECURITY_CREDENTIALS, "");
                }
            }
            if (!isEmpty(this.getProtocol())) {
                env.put(Context.SECURITY_PROTOCOL, this.getProtocol());
            }

            // Get the SSLContext used by the bespoke socket factory
            SSLContext sslContext = this.getSSLContext();

            // Create the SSLSocketFactory 
            LDAPSocketFactory.setSSLContext(sslContext);
            env.put("java.naming.ldap.factory.socket", LDAPSocketFactory.class.getName());
            env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
            ret = new InitialLdapContext(env, null);
        } catch (NamingException ex) {
            Exceptions.printStackTrace(ex);
        }        
        return ret;
    }

    /**
     * To get a TLS Connection with the LDAP Server, we create a simple
     * connection then negotiate a TLS Session with the server.
     *
     * @return LDAP Context
     */
    private LdapContext createTLSConnection() {
        LdapContext tlsContext = null;
        try {
            tlsContext = this.createSimpleConnection();
            // Let's create the SSL Context that will be used during the negociation
            SSLContext sslContext = this.getSSLContext();
            LDAPSocketFactory.setSSLContext(sslContext);
            SSLSocketFactory sslSocketFactory = (SSLSocketFactory) LDAPSocketFactory.getDefault();
            StartTlsResponse response = (StartTlsResponse) tlsContext.extendedOperation(new StartTlsRequest());
            response.negotiate(sslSocketFactory);
        } catch (NamingException | IOException ex) {
            Exceptions.printStackTrace(ex);
        }
        return tlsContext;
    }

    /**
     * The method getSSLContext create an SSL Context that use a bespoke
     * SSLSocketFactory and does not use the default one used by the JVM. It
     * allows the use of additional KeyStores and TrustStores
     *
     * @return SSLContext
     */
    private SSLContext getSSLContext() {
        String keystoreFileName, keystorePW, keystoreType;
        String truststoreFileName, truststorePW, truststoreType;
        try {
            if (!isEmpty(this.getKeystore())) {
                keystoreFileName = this.getKeystore();
            } else {
                keystoreFileName = System.getProperty("javax.net.ssl.keyStore");
            }
            mLogger.fine(MessageFormat.format("the connection uses {0} as keystore file", keystoreFileName));

            // Search for the keystore password
            if (!isEmpty(this.getKeystorepassword())) {
                keystorePW = this.getKeystorepassword();
            } else {
                keystorePW = System.getProperty("javax.net.ssl.keyStorePassword");
            }
            mLogger.fine("Keystore password read but none disclosed in the log");

            // Search for the keystore type 
            if (!isEmpty(this.getKeystoretype())) {
                keystoreType = this.getKeystoretype();
            } else {
                keystoreType = "JKS";
            }
            mLogger.fine(MessageFormat.format("The keystore type is :  {0}", keystoreType));

            if (!isEmpty(this.getTruststore())) {
                truststoreFileName = this.getTruststore();
            } else {
                truststoreFileName = System.getProperty("javax.net.ssl.trustStore");
            }
            mLogger.fine(MessageFormat.format("the connection uses {0} as truststore file", keystoreFileName));

            // Search for the truststore password
            if (!isEmpty(this.getTruststorepassword())) {
                truststorePW = this.getTruststorepassword();
            } else {
                truststorePW = System.getProperty("javax.net.ssl.trustStorePassword");
            }
            mLogger.fine("TrustStore password read but none disclosed in the log");

            // search for the truststoretype
            if (!isEmpty(this.getTruststoretype())) {
                truststoreType = this.getTruststoretype();
            } else {
                truststoreType = "JKS";
            }
            mLogger.fine(MessageFormat.format("The trustStore type is :  {0}", truststoreType));

            // Create SSL Context in many Steps 
            // Create KeyManager factory      
            mLogger.fine("Create SSL KeyManager");
            InputStream isKey = new FileInputStream(keystoreFileName);
            String password = keystorePW;
            KeyStore myKeystore = KeyStore.getInstance(keystoreType);
            myKeystore.load(isKey, keystorePW.toCharArray());
            String defaultAlgorithm = KeyManagerFactory.getDefaultAlgorithm();
            KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            kmf.init(myKeystore, password.toCharArray());
            KeyManager[] keyManagers = kmf.getKeyManagers();
            mLogger.fine("SSL KeyManager created successfully ");

            // Create Trusrmanager Factory
            mLogger.fine("Create SSL TrustManager");
            InputStream isTrust = new FileInputStream(truststoreFileName);
            KeyStore myTrustStore = KeyStore.getInstance(truststoreType);
            myTrustStore.load(isTrust, truststorePW.toCharArray());
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            tmf.init(myKeystore);
            TrustManager[] trustManagers = tmf.getTrustManagers();
            mLogger.fine("SSL TrustManager created successfully ");

            // Create Secure Random 
            mLogger.fine("Create Secure Random");
            SecureRandom random = SecureRandom.getInstanceStrong();
            mLogger.fine(MessageFormat.format("Create SSL Context with {0} as protocol", this.getProtocol()));
            SSLContext sslContext = SSLContext.getInstance(this.getProtocol());
            sslContext.init(keyManagers, trustManagers, random);
            mLogger.fine("SSL Context created");
            return sslContext;

        } catch (IOException | KeyManagementException | KeyStoreException | NoSuchAlgorithmException | UnrecoverableKeyException | CertificateException ex) {
            mLogger.fine("SSL Context creation failed");
            Exceptions.printStackTrace(ex);
        }
        return null;
    }

    /**
     * Use to close the existring connection and does not consume connection
     * from the pool
     */
    public void closeConnection() {
        try {
            if (connection != null) {
                connection.close();
//                CONNECTION_CLOSED_COUNT++;
                connection = null;
                mLogger.fine("Close current Connection successful");
            }
        } catch (Exception ex) {
            mLogger.fine("Unable to close the current Connection");
            ex.printStackTrace();
        }
    }

//    /**
//     * There are many steps to create the connection. With the element meta 1-
//     * We set first the Map env for the Context 2- Since we want to use
//     * alternative KeyStore and Trutstore, We use a bespoke Socket Factory that
//     * will be used by the LDAP Context to take into account the certificate
//     * defined in the WSDL. FYI we need a Truststore for SSL handshake but also
//     * a KeyStore for a mutual identification that can be required by the LDAP
//     * server. 3- we create a SSL Context to create a Socket Factory that takes
//     * into account the Key and TrustStore. 4- The Socket Factory creation is
//     * invoked by the LDAP Context init(env)
//     *
//     * Information can be found here :
//     * https://docs.oracle.com/javase/8/docs/technotes/guides/jndi/jndi-ldap.html#PROP
//     * https://docs.oracle.com/javase/jndi/tutorial/ldap/index.html
//     * https://ldapwiki.com/wiki/Main
//     */
//}
//private LdapContext createSSLConnection() {
//        LdapContext ret = null;
//        try {
//            if (!this.isEmptyOrNull(this.getTruststore())) {
//                System.setProperty("javax.net.ssl.trustStore", this.getTruststore());
//            }
//            if (!isEmptyOrNull(this.getTruststoretype())) {
//                System.setProperty("javax.net.ssl.trustStoreType", this.getTruststoretype());
//            }
//            if (!isEmptyOrNull(this.getTruststorepassword())) {
//                System.setProperty("javax.net.ssl.trustStorePassword", this.getTruststorepassword());
//            }
//            if (!isEmptyOrNull(this.getKeystore())) {
//                System.setProperty("javax.net.ssl.keyStore", this.getKeystore());
//            }
//            if (!isEmptyOrNull(this.getKeystorepassword())) {
//                System.setProperty("javax.net.ssl.keyStorePassword", this.getKeystorepassword());
//            }
//            if (!isEmptyOrNull(this.getKeystoreusername())) {
//                System.setProperty("javax.net.ssl.keyStoreUsername", this.getKeystoreusername());
//            }
//            if (!isEmptyOrNull(this.getKeystoretype())) {
//                System.setProperty("javax.net.ssl.keyStoreType", this.getKeystoretype());
//            }
//
//            Hashtable<String, String> env = new Hashtable<String, String>();
//            if (!isEmptyOrNull(this.getPrincipal())) {
//                env.put(Context.SECURITY_PRINCIPAL, this.getPrincipal());
//            }
//            if (!isEmptyOrNull(this.getCredential())) {
//                env.put(Context.SECURITY_CREDENTIALS, this.getCredential());
//            }
//            env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
//            env.put(Context.PROVIDER_URL, this.getLocation());
//            if (!isEmptyOrNull(this.getAuthentication())) {
//                env.put(Context.SECURITY_AUTHENTICATION, this.getAuthentication());
//            }
//            if (!isEmptyOrNull(this.getProtocol())) {
//                env.put(Context.SECURITY_PROTOCOL, this.getProtocol());
//            }
//
//            ret = new InitialLdapContext(env, null);
//            if (this.getTlssecurity().toUpperCase().equals("YES")
//                    || LdapConnection.SSL_TYPE_TLS.equals(this.getSsltype())) {
//                StartTlsResponse tls = (StartTlsResponse) ret.extendedOperation(new StartTlsRequest());
//                tls.negotiate();
//            }
//            connection = ret;
//            curConnectionType = this.getSsltype();
//            connectionReconnect = false;
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//
//        return ret;
//    }
//    /**
//     * The method has been completely reviewed to fix an incompatibility between
//     * LDAP BC and HTTP BC. LDAP BC designer wanted to used default socket
//     * factory and consequently setup JVM properties with alternative
//     * Certificates. To solve this issue in this version we create a bespoke
//     * socket factory. The factory is created with a SSLContext that use
//     * alternative certificates as parameters
//     *
//     * @param meta
//     * @param mKeyStoreUtil
//     * @return
//     * @throws Exception
//     */
//    @SuppressWarnings("empty-statement")
//        public LdapContext getConnection02() {
//        LdapContext context = null;
//        /**
//         * Check if the connection already exists and there is no need for
//         * recreating a connection. We recreate a connection when message
//         * properties modify the connection properties. See BPEL Mapper
//         */
//        if (null != connection && curConnectionType.equals(this.getSsltype()) && !connectionReconnect) {
//            return connection;
//        }
//        closeConnection();
//
//        // Select the connection mode Normal SSL or TLS 
//        if (this.getSsltype().equals(LdapConnection_original.SSL_TYPE_NONE)) {
//            return this.creatSimpleConnection();
//        }
//
//        if (this.getSsltype().equals(LdapConnection_original.SSL_TYPE_SSL)) {
//            return this.createSSLConnection();
//        }
//        this.getSsltype();
//        if (this.getSsltype().equals(LdapConnection_original.SSL_TYPE_TLS)) {
//            return this.createSSLConnection();
//        }
//
//        return null;
//    }
//
//    // we are in the case we need to create a new connection. 
//    /**
//     * There are many steps to create the connection. With the element meta 1-
//     * We set first the Map env for the Context 2- Since we want to use
//     * alternative KeyStore and Trutstore, We use a bespoke Socket Factory that
//     * will be used by the LDAP Context to take into account the certificate
//     * defined in the WSDL. FYI we need a Truststore for SSL handshake but also
//     * a KeyStore for a mutual identification that can be required by the LDAP
//     * server. 3- we create a SSL Context to create a Socket Factory that takes
//     * into account the Key and TrustStore. 4- The Socket Factory creation is
//     * invoked by the LDAP Context init(env)
//     *
//     * Information can be found here :
//     * https://docs.oracle.com/javase/8/docs/technotes/guides/jndi/jndi-ldap.html#PROP
//     * https://docs.oracle.com/javase/jndi/tutorial/ldap/index.html
//     * https://ldapwiki.com/wiki/Main
//     */
//    // Let start by seeting the Map for the LDAP Context Creation 
//    Hashtable<String, String> env = new Hashtable<String, String>();
//
//    // The context class is com.sun.jndi.ldap.LdapCtxFactory 
//    env.put (Context.INITIAL_CONTEXT_FACTORY,  "com.sun.jndi.ldap.LdapCtxFactory");
//
//        // Context.PROVIDER_URL contains host, port and directory entry
//        // The LDAP wizard does not allow multiple URL.
//        if (!isEmpty( 
//        this.getLocation())) {
//            mLogger.log(Level.INFO, mMessages.getString("LDAPBC_CFG0215_URL", this.getLocation()));
//        env.put(Context.PROVIDER_URL, this.getLocation());
//    }
//
//    // Context.SECURITY_PRINCIPAL is the login to access to the server (ex: "uid=admin,ou=sytem") 
//    if (!isEmpty( 
//        this.getPrincipal())) {
//            mLogger.log(Level.INFO, mMessages.getString("LDAPBC_CFG0216_Principal", this.getPrincipal()));
//        env.put(Context.SECURITY_PRINCIPAL, this.getPrincipal());
//    }
//
//    // Context.SECURITY_CREDENTIALS is the password associated to Context.SECURITY_PRINCIPAL
//    if (!isEmpty( 
//        this.getCredential())) {
//            mLogger.log(Level.INFO, mMessages.getString("LDAPBC_CFG0217_Credential", mKeyStoreUtil.encrypt(this.getCredential())));
//        env.put(Context.SECURITY_CREDENTIALS, this.getCredential());
//    }
//
//    /**
//     * There are three options for the authentication: none, simple, strong None
//     * does not send a login password Simple send a login password. It must be
//     * used when the identification is required. In this version strong means
//     * DIGEST-MD5. Today 2021, DIGEST-MD5 is not the best SASL mechanism. Must
//     * be changed in the next version
//     *
//     * Context.SECURITY_AUTHENTICATION values can be none, simple or strong. It
//     * cannot be "none simple" or "simple strong" . These combination can be
//     * used for the connection pools:
//     * System.setProperty("com.sun.jndi.ldap.connect.pool.authentication,"")
//     */
//    if (!isEmpty( 
//        this.getAuthentication())) {
//            mLogger.log(Level.INFO, mMessages.getString("LDAPBC_CFG0218_Authentication", this.getAuthentication()));
//        // Change the authentication strong with DIGEST-MD5
//        if (this.getAuthentication().contentEquals("strong")) {
//            String replace = this.getAuthentication().replace("strong", "DIGEST-MD5");
//            this.setAuthentication(replace);
//        }
//        env.put(Context.SECURITY_AUTHENTICATION, this.getAuthentication());
//        mLogger.log(Level.INFO, mMessages.getString("LDAPBC_CFG0218A_Authentication", this.getAuthentication()));
//    }
//
//    /**
//     * There are three values for the referral
//     * https://docs.oracle.com/javase/jndi/tutorial/ldap/referral/jndi.html
//     * follow: Automatically follow any referrals ignore: Ignore referrals
//     * (default) throw: Throw a ReferralException for each referral The referal
//     * can be redifined at the message level
//     */
//    if (!isEmpty(meta.getReferral 
//        ())) {
//            mLogger.log(Level.INFO, mMessages.getString("LDAPBC_CFG0232_Referral", meta.getReferral()));
//        env.put(Context.REFERRAL, meta.getReferral());
//    }
//
//    /**
//     * Values for Context.SECURITY_PROTOCOL can be empty or "ssl"
//     */
//    if (!isEmpty( 
//        this.getProtocol())) {
//            mLogger.log(Level.INFO, mMessages.getString("LDAPBC_CFG0219_Protocol", this.getProtocol()));
//        env.put(Context.SECURITY_PROTOCOL, this.getProtocol());
//    }
//
//    /**
//     * SSL and TLS require to setup an SSL context that will be used to create
//     * an accurate socket factory. This bespoke socket factory does not modify
//     * the System.properties and then don't impact the other component that used
//     * the system keystore and trustore If keystore and truststore element are
//     * not set, we use the one defined by default with the JVM
//     *
//     */
//    // define the initial context
//    InitialLdapContext context = null;
//
//    /**
//     * If SSL or TLS Mode is selected we need to create an SSL context Reminder
//     * that we cannot have has the same time SLL and TLS but we need to have a
//     * SSLContext in both case.
//     */
//    SSLContext sslContext = null;
//
//    if (this.getSsltype() 
//        .equalsIgnoreCase(SSL_TYPE_SSL) || this.getSsltype().equalsIgnoreCase(SSL_TYPE_TLS)) {
//            // Search for the Keystore file, Keystore password and type  
//            String keystoreFileName, keystorePW, keystoreType;
//
//        if (!isEmptyOrNull(this.getKeystore())) {
//            keystoreFileName = this.getKeystore();
//        } else {
//            keystoreFileName = System.getProperty("javax.net.ssl.keyStore");
//        }
//        mLogger.log(Level.INFO, mMessages.getString("LDAPBC_CFG0223_Key_Store", keystoreFileName));
//
//        // Search for the keystore password
//        if (!isEmptyOrNull(this.getKeystorepassword())) {
//            keystorePW = this.getKeystorepassword();
//        } else {
//            keystorePW = System.getProperty("javax.net.ssl.keyStorePassword");
//        }
//        mLogger.log(Level.INFO, mMessages.getString("LDAPBC_CFG0224_Key_Store_Pwd", mKeyStoreUtil.encrypt(keystorePW)));
//
//        // Search for the keystore type 
//        if (!isEmptyOrNull(this.getKeystoretype())) {
//            keystoreType = this.getKeystoretype();
//        } else {
//            keystoreType = "JKS";
//        }
//        mLogger.log(Level.INFO, mMessages.getString("LDAPBC_CFG0226_Key_Store_Type", keystoreType));
//
//        // Search for the Trustore file, Truststore password and type  
//        String truststoreFileName, truststorePW, truststoreType;
//
//        if (!isEmptyOrNull(this.getTruststore())) {
//            truststoreFileName = this.getTruststore();
//        } else {
//            truststoreFileName = System.getProperty("javax.net.ssl.trustStore");
//        }
//        mLogger.log(Level.INFO, mMessages.getString("LDAPBC_CFG0220_Trust_Store", truststoreFileName));
//
//        // Search for the truststore password
//        if (!isEmptyOrNull(this.getTruststorepassword())) {
//            truststorePW = this.getTruststorepassword();
//        } else {
//            truststorePW = System.getProperty("javax.net.ssl.trustStorePassword");
//        }
//        mLogger.log(Level.INFO, mMessages.getString("LDAPBC_CFG0221_Trust_Store_Pwd", mKeyStoreUtil.encrypt(truststorePW)));
//
//        // search for the truststoretype
//        if (!isEmptyOrNull(this.getTruststoretype())) {
//            truststoreType = this.getTruststoretype();
//        } else {
//            truststoreType = "JKS";
//        }
//
//        // Create Keystore factory and KeyManager 
//        mLogger.log(Level.FINE, "Start creating KeyManager");
//        InputStream isKey = new FileInputStream(keystoreFileName);
//        KeyStore myKeystore = KeyStore.getInstance(keystoreType);
//        myKeystore.load(isKey, truststorePW.toCharArray());
//        KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
//        kmf.init(myKeystore, truststorePW.toCharArray());
//        KeyManager[] keyManagers = kmf.getKeyManagers();
//        mLogger.log(Level.FINE, "KeyManager created");
//
//        // Create Trustmanager Factory
//        mLogger.log(Level.FINE, "Start creating TrustManager");
//        InputStream isTrust = new FileInputStream(truststoreFileName);
//        KeyStore myTrustStore = KeyStore.getInstance(KeyStore.getDefaultType());
//        myTrustStore.load(isTrust, truststorePW.toCharArray());
//        TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
//        tmf.init(myKeystore);
//        TrustManager[] trustManagers = tmf.getTrustManagers();
//        mLogger.log(Level.FINE, "TrustManager created");
//
//        // Create Secure Random 
//        SecureRandom random = SecureRandom.getInstanceStrong();
//        mLogger.log(Level.FINE, "Secure Random created");
//
//        // Create the SSL context and pass the protocol as parameter in the constructor
//        String protocol = this.getProtocol();
//        sslContext = SSLContext.getInstance(protocol);
//        sslContext.init(keyManagers, trustManagers, random);
//        mLogger.log(Level.INFO, "Bespoke SSL Context Created");
//    }
//
//    /**
//     * Setting up the default SSLContext requires us to use the
//     * SSLContext.setDefault() method Them to invoke the new
//     * InitialLdapContext(env, null). The method get connection can be invoke by
//     * many threads. We don't want the SSLDefault to be modified between both
//     * instructions. For that reason, We lock this piece of code. There is no
//     * impact on the performance since the connection is created during the SU
//     * Deployment.
//     */
//    /**
//     * SSL is chosen. We setup the context with SSL elements
//     */
//    if (this.getSsltype() 
//        .equalsIgnoreCase(SSL_TYPE_SSL)) {
//            lock.lock();
//        try {
//            // Setup SocketFactory SSLContext
//            LDAPSocketFactory.setSSLContext(sslContext);
//            // Setup Socket factory for the environement 
//            env
//
//
//
//
//
//
//
//
//
//.put("java.naming.ldap.factory.socket", LDAPSocketFactory.class
//
//
//
//
//.getName());
//            // Retry to connect with delay between retries
//            int retryInterval = meta.getRetryInterval();
//            int retryCount = meta.getRetryCount();
//            for (int retry = 0; retry <= retryCount; retry++) {
//                try {
//                    context = new InitialLdapContext(env, null);
//                    mLogger.log(Level.INFO, "LDAP InitialContext created");
//                    connection = context;
//                
//
//
//
//
//
//
//
//
//
//} catch (javax.naming.CommunicationException conEx) {
//                    try {
//                        if (retry < retryCount) {
//                            Logger.getLogger(LdapConnection.class
//
//
//
//
//.getName()).log(Level.SEVERE, "Failed to establish connection : {0}", retry);
//                            Logger
//
//
//
//
//
//
//
//
//
//.getLogger(LdapConnection.class
//
//
//
//
//.getName()).log(Level.SEVERE, "+++ Unable to establish connection ... will retry after: {0} +++", retryInterval);
//                        }
//                        Thread.sleep(retryInterval);
//                    } catch (InterruptedException e) {
//                        ;
//                    
//
//
//
//
//
//
//
//
//
//}
//                    if (retry == retryCount) {
//                        Logger.getLogger(LdapConnection.class
//
//
//
//
//.getName()).log(Level.SEVERE, "Cannot establish connection after {0}times  check the external system for Connection", retry);
//                        Logger
//
//
//
//
//
//
//
//
//
//.getLogger(LdapConnection.class
//
//
//
//
//.getName()).log(Level.SEVERE, conEx.getMessage(), conEx);
//                    }
//                }
//                if (null != context) {
//                    break;
//                }
//            }
//        } finally {
//            lock.unlock();
//        }
//        return context;
//    }
//
//    /**
//     * Case no SSL. We create the context without SSL. If TLS we negotiate a TLS
//     * session after the creation of the context
//     */
//    //   Retry to connect with delay between retries
//    int retryInterval = meta.getRetryInterval();
//    int retryCount = meta.getRetryCount();
//    for (int retry = 0;
//    retry <= retryCount ;
//    retry
//
//    
//        ++) {
//            try {
//            context = new InitialLdapContext(env, null);
//            mLogger.log(Level.INFO, "LDAP InitialContext created");
//            connection = context;
//        
//
//
//
//
//
//
//
//
//
//} catch (javax.naming.CommunicationException conEx) {
//            try {
//                if (retry < retryCount) {
//                    Logger.getLogger(LdapConnection.class
//
//
//
//
//
//.getName()).log(Level.SEVERE, "Failed to establish connection : {0}", retry);
//                    Logger
//
//
//
//
//
//
//
//
//
//.getLogger(LdapConnection.class
//
//
//
//
//
//.getName()).log(Level.SEVERE, "+++ Unable to establish connection ... will retry after: {0} +++", retryInterval);
//                }
//                Thread.sleep(retryInterval);
//            } catch (InterruptedException e) {
//                ;
//            
//
//
//
//
//
//
//
//
//
//}
//            if (retry == retryCount) {
//                Logger.getLogger(LdapConnection.class
//
//
//
//
//
//.getName()).log(Level.SEVERE, "Cannot establish connection after {0}times  check the external system for Connection", retry);
//                Logger
//
//
//
//
//
//
//
//
//
//.getLogger(LdapConnection.class
//
//
//
//
//
//.getName()).log(Level.SEVERE, conEx.getMessage(), conEx);
//            }
//        }
//        if (null != context) {
//            break;
//        }
//    }
//
//    // Etablish TLS Security if needed
//    if (this.getSsltype() 
//        .equals(LdapConnection.SSL_TYPE_TLS)) {
//            lock.lock();
//        try {
//            // Create a new Socket factory
//            LDAPSocketFactory.setSSLContext(sslContext);
//            SSLSocketFactory socketFactory = (SSLSocketFactory) LDAPSocketFactory.getDefault();
//            mLogger.log(Level.INFO, mMessages.getString("LDAPBC_CFG0227_Tls_Security", this.getTlssecurity()));
//            mLogger.log(Level.INFO, mMessages.getString("LDAPBC_CFG0217_Ssl_Type", this.getSsltype()));
//            StartTlsResponse tls = (StartTlsResponse) context.extendedOperation(new StartTlsRequest());
//            tls.negotiate(socketFactory);
//            mLogger.log(Level.INFO, "LDAP TLS negociation succesful");
//        } catch (Exception ex) {
//            mLogger.log(Level.INFO, "Unable To etablish a LDAP TLS negociation");
//        } finally {
//            lock.unlock();
//        }
//    }
//    connection  = context;
//    return context ;
//}
    public ArrayList getDNs() {
        ArrayList<String> list = new ArrayList<String>();
        try {
            DirContext ctx = getConnection();
            SearchControls constraints = new SearchControls();
            constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
            NamingEnumeration results = ctx.search(this.getDn(), "(ObjectClass=*)", constraints);
            while (null != results && results.hasMore()) {
                SearchResult sr = (SearchResult) results.next();
                list.add(sr.getNameInNamespace());
                sr = null;
            }
        } catch (NamingException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public List getObjectNames() throws NamingException {
        List<String> ret = new ArrayList<String>();
        DirContext top = getConnection().getSchema("");
        NamingEnumeration ne = top.list("ClassDefinition");

        while (ne.hasMore()) {
            NameClassPair pair = (NameClassPair) ne.next();
            String clsName = pair.getName();
            ret.add(clsName);
        }

        return ret;
    }

    public LdifObjectClass getObjectClass(String objName) throws NamingException {
        LdifObjectClass objClass = new LdifObjectClass();

        DirContext top = getConnection().getSchema("");
        DirContext schema = (DirContext) top.lookup("ClassDefinition/" + objName);
        Attributes atrs = schema.getAttributes("");
        Attribute name = atrs.get("NAME");
        NamingEnumeration nameValue = name.getAll();
        objClass.setName((String) nameValue.next());
        objClass.setLdapUrl(this.getLocation());

        Attribute sup = atrs.get("SUP");
        if (sup != null) {
            NamingEnumeration supValue = sup.getAll();
            String sups = "";
            while (supValue.hasMore()) {
                String a = (String) supValue.next();
                sups += a + ", ";
            }
            if (sups.length() > 1) {
                sups = sups.substring(0, sups.length() - 2);
            }
            objClass.setSuper(sups);
        }

        Attribute may = atrs.get("MAY");
        if (may != null) {
            NamingEnumeration mayValue = may.getAll();
            while (mayValue.hasMore()) {
                String a = (String) mayValue.next();
                objClass.addMay(a);
            }
        }

        Attribute must = atrs.get("MUST");
        if (must != null) {
            NamingEnumeration mustValue = must.getAll();
            while (mustValue.hasMore()) {
                String a = (String) mustValue.next();
                objClass.addMust(a);
            }
        }
        objClass.setSelected(new ArrayList());
        return objClass;
    }

    public boolean isSingleValue(String attrName) {
        try {
            DirContext schema = getConnection().getSchema("");
            DirContext attrSchema = (DirContext) schema.lookup("AttributeDefinition/" + attrName);
            Attributes a = attrSchema.getAttributes("");
            NamingEnumeration ane = a.getAll();

            while (ane.hasMore()) {
                Attribute attr = (Attribute) ane.next();
                String attrType = attr.getID();
                NamingEnumeration values = attr.getAll();

                while (values.hasMore()) {
                    Object oneVal = values.nextElement();
                    if (oneVal instanceof String) {
                        if (attrType.equals("SINGLE-VALUE")) {
                            return Boolean.parseBoolean(oneVal.toString());
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean validateParam() {
        if (!connectionReconnect) {
            return true;
        }
        try {
            if (!isEmpty(this.getTruststore())) {
                System.setProperty("javax.net.ssl.trustStore", this.getTruststore());
            }
            if (!isEmpty(this.getTruststoretype())) {
                System.setProperty("javax.net.ssl.trustStoreType", this.getTruststoretype());
            }
            if (!isEmpty(this.getTruststorepassword())) {
                System.setProperty("javax.net.ssl.trustStorePassword", this.getTruststorepassword());
            }
            if (!isEmpty(this.getKeystore())) {
                System.setProperty("javax.net.ssl.keyStore", this.getKeystore());
            }
            if (!isEmpty(this.getKeystorepassword())) {
                System.setProperty("javax.net.ssl.keyStorePassword", this.getKeystorepassword());
            }
            if (!isEmpty(this.getKeystoreusername())) {
                System.setProperty("javax.net.ssl.keyStoreUsername", this.getKeystoreusername());
            }
            if (!isEmpty(this.getKeystoretype())) {
                System.setProperty("javax.net.ssl.keyStoreType", this.getKeystoretype());
            }

            Hashtable<String, String> env = new Hashtable<String, String>();
            if (!isEmpty(this.getPrincipal())) {
                env.put(Context.SECURITY_PRINCIPAL, this.getPrincipal());
            }
            if (!isEmpty(this.getCredential())) {
                env.put(Context.SECURITY_CREDENTIALS, this.getCredential());
            }
            env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
            env.put(Context.PROVIDER_URL, this.getLocation());
            if (!isEmpty(this.getAuthentication())) {
                env.put(Context.SECURITY_AUTHENTICATION, this.getAuthentication());
            }
            if (!isEmpty(this.getProtocol())) {
                env.put(Context.SECURITY_PROTOCOL, this.getProtocol());
            }

            LdapContext ret = new InitialLdapContext(env, null);
            if (this.getTlssecurity().toUpperCase().equals("YES") || LdapConnection.SSL_TYPE_TLS.equals(this.getSsltype())) {
                StartTlsResponse tls = (StartTlsResponse) ret.extendedOperation(new StartTlsRequest());
                tls.negotiate();
                tls.close();
            }
//            CONNECTION_CREATED_COUNT++;
            if (ret != null) {
                ret.close();
//                CONNECTION_CLOSED_COUNT++;
                ret = null;
            }
        } catch (Exception ex) {
            return false;
        }
        return true;
    }

    public boolean isConnectionReconnect() {
        return connectionReconnect;
    }

    public void setConnectionReconnect(boolean connectionReconnect) {
        this.connectionReconnect = connectionReconnect;
    }

//    public String toString() {
//        String ret = "";
//        ret += "localtion: " + this.getLocation() +
//                "\n principal: " + this.getPrincipal() +
//                "\n credential: " + this.getCredential() +
//                "\n ssltype: " + this.getSsltype() +
//                "\n authentication: " + this.getAuthentication() +
//                "\n protocol: " + this.getProtocol() +
//                "\n truststore: " + this.getTruststore() +
//                "\n truststorepassword: " + this.getTruststorepassword() +
//                "\n truststoretype: " + this.getTruststoretype() +
//                "\n keystorepassword: " + this.getKeystorepassword() +
//                "\n keystoreusername: " + this.getKeystoreusername() +
//                "\n keystore: " + this.getKeystore() +
//                "\n keystoretype: " + this.getKeystoretype() +
//                "\n tlssecurity: " + this.getTlssecurity()+
//                "\n isReconnect"+this.connectionReconnect;
//        
//        return ret;
//    }
}
