package net.openesb.netbeans.module.server.support.jboss.nodes.actions;

import org.openide.nodes.Node;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public interface RefreshCookie extends Node.Cookie {
    
    public void refresh();    
}
