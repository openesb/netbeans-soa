package org.openesb.netbeans.modules.appui.welcome;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;


/**
 *
 * @author David BRASSELY
 */
class HorizontalImagePanel extends FixedImagePanel {
    
    private Dimension minSize;
    private Dimension maxSize;
    
    public HorizontalImagePanel(Image img) {
        super(img);
        
        minSize = new Dimension(0, imageSize.height);
        maxSize = new Dimension(Integer.MAX_VALUE, imageSize.height);
    }

    @Override
    public Dimension getPreferredSize() {
        Component[] childComponents = getComponents();
        if (childComponents.length == 0) {
            return minSize;
        } else {
            // TODO: should be computed just when child components are added/removed
            int minWidth = 0;
            for (Component component : childComponents) {
                int minChildComponentWidth = component.getMinimumSize().width;
                if (minWidth < minChildComponentWidth) minWidth = minChildComponentWidth;
            }
            return new Dimension(minWidth, imageSize.height);
        }
    }
    
    @Override
    public Dimension getMaximumSize() {
        return maxSize;
    }
    
    
    @Override
    protected void paintComponent(Graphics graphics) {
        int compEnd = getSize().width;
        int drawEnd = 0;
        while (drawEnd < compEnd) {
            graphics.drawImage(image, drawEnd, 0, this);
            drawEnd += imageSize.width;
        }
    }
}
