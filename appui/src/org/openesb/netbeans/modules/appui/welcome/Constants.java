package org.openesb.netbeans.modules.appui.welcome;

import java.awt.BasicStroke;
import java.awt.Font;
import java.awt.Stroke;

/**
 *
 * @author David BRASSELY
 */
public interface Constants {

    static final String COLOR_SCREEN_BACKGROUND = "ScreenBackgroundColor"; //NOI18N
    static final String COLOR_CONTENT_BACKGROUND = "ContentBackgroundColor"; //NOI18N
    static final String BORDER_COLOR = "BorderColor"; //NOI18N
    
    
    static final int FONT_SIZE = Utils.getDefaultFontSize();
    static final Font BUTTON_FONT = new Font( null, Font.BOLD, FONT_SIZE );

    static final Stroke LINK_IN_FOCUS_STROKE = new BasicStroke(1, BasicStroke.CAP_SQUARE,
        BasicStroke.JOIN_BEVEL, 0, new float[] {0, 2}, 0);
    static final String LINK_IN_FOCUS_COLOR = "LinkInFocusColor"; //NOI18N
    static final String LINK_COLOR = "LinkColor"; //NOI18N
    static final String MOUSE_OVER_LINK_COLOR = "MouseOverLinkColor"; //NOI18N
    static final String VISITED_LINK_COLOR = "VisitedLinkColor"; //NOI18N

    
    static final int START_PAGE_MIN_WIDTH = 600;
}
